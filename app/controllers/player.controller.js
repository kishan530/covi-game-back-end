const db = require("../models");
const Player = db.players;
const Game = db.games;
const Log = db.logs;

// Create and Save a new Player
exports.create = (req, res) => {
    // Validate request
    if (!req.body.name) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
  
    // Create a Player
    const player = new Player({
      full_name: req.body.name,
      mobile: req.body.mobile,
      email: req.body.email,
      password: req.body.password,
      avatar: req.body.avatar,
      active: req.body.active ? req.body.active : true
    });
  
    // Save Player in the database
    player
      .save(player)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Player."
        });
      });
  };

  // Create and Save a new Game
exports.startGame = (req, res) => {
  // Validate request
  if (!req.body.player_id) {
    res.status(400).send({ message: "player_id can not be empty!" });
    return;
  }

  // Create a Player
  const game = new Game({
    date: new Date(),
    duration: req.body.duration,
    winner: req.body.winner,
    winDuration: req.body.winDuration,
    playerLife:req.body.playerLife,
    covidLife:req.body.covidLife,
    player:req.body.player_id
  });


  // Save Player in the database
  game
    .save(game)
    .then(data => {
    
      Player.findById(req.body.player_id)
      .then(data => {
        if (!data)
          res.status(404).send({ message: "Not found Player with id " + req.body.player_id });
        else 
        data.games.push(game);
        data.save();
       // res.send(data);
      })
      .catch(err => {
        res
          .status(500)
          .send({ message: "Error retrieving Player with id=" + req.body.player_id });
      }); 
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Player."
      });
    });

};

  // Create and Save a new gameAction
  exports.gameAction = (req, res) => {
    // Validate request
    if (!req.body.game_id) {
      res.status(400).send({ message: "game_id can not be empty!" });
      return;
    }
  
    var life = Math.floor(Math.random() * 10)+1;
    var type = req.body.type;
    var text = "";
    var playerLife = req.body.playerLife;
    var covidLife = req.body.covidLife;
    switch(type){
      case 'attack':
         text = req.body.name+" "+req.body.type+" "+req.body.attacked+" by "+life;
         if(req.body.name!='Dragon'){
          covidLife = covidLife-life;
         }else{
           playerLife = playerLife-life;
         }
         break;
      case 'blast':
          text = req.body.name+" "+req.body.type+" "+req.body.attacked+" by "+life;
          if(req.body.name!='Dragon'){
           covidLife = covidLife-life;
          }else{
            playerLife = playerLife-life;
          }
          break;
      case 'heal':
          text = req.body.name+" performed healing by "+life;
          if(req.body.name!='Dragon'){
            playerLife = parseInt(covidLife)+parseInt(life);
          }
          break;
      case 'giveUp':
          text = req.body.name+" giveup to "+req.body.attacked;
          if(req.body.name!='Dragon'){
            playerLife = 0;
          }
          break;
      case 'timeout':
          text = "Game timeout";
          break;
    }
   
    // Create a Player
    const log = new Log({
      text:text,
      playerLife:playerLife,
      covidLife:covidLife,
      type:req.body.type,
      game:req.body.game_id
    });
  
  
    // Save Player in the database
    log
      .save(log)
      .then(data => {
      
        Game.findById(req.body.game_id).populate({path: 'logs', select: 'text'})
        .then(data => {
          if (!data)
            res.status(404).send({ message: "Not found Game with id " + req.body.game_id });
          else
          data.playerLife = playerLife;
          data.covidLife = covidLife;


          if(type=='timeout'){
            if(parseInt(playerLife)<parseInt(covidLife)){
              data.winner = 'Dragon';
            }else{
              data.winner = req.body.name;
            }
          }else{
            if(type=='giveUp'){
              data.winner = 'Dragon';
            }else{
              if(playerLife<=0 || covidLife<=0){
                data.winner = req.body.name;
              }
            }
          }
          if(data.winner){
            data.winDuration = req.body.winDuration;
          }
          
          data.logs.push(log);
          data.save();
          res.send(data);
        })
        .catch(err => {
          res
            .status(500)
            .send({ message: "Error retrieving Game with id=" + req.body.game_id });
        }); 
        //res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Player."
        });
      });
  
  };


// Find a single Player with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
     Player.findById(id).populate({path: 'games', select: 'winner date duration winDuration'})
      .then(data => {
        if (!data)
          res.status(404).send({ message: "Not found Player with id " + id });
        else res.send(data);
      })
      .catch(err => {
        res
          .status(500)
          .send({ message: "Error retrieving Player with id=" + id });
      }); 
};

// Delete all Players from the database.
exports.deleteAll = (req, res) => {
  
};