const players = require("../controllers/player.controller.js");
var router = require("express").Router();
const { authJwt } = require("../middlewares");
module.exports = app => {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

    // Create a new players
    router.post("/", players.create);

    // Create a new game
    router.post("/start-game",[authJwt.verifyToken], players.startGame);

    // Create a new game action
    router.post("/game-action",[authJwt.verifyToken], players.gameAction);
  
    // Retrieve a single player with id
    router.get("/:id",[authJwt.verifyToken], players.findOne);
  
  
    app.use('/api/players', router);
  };