const Game = require('./game.model.js');

  module.exports = mongoose => {
    var schema = mongoose.Schema(
      {
        full_name: String,
        mobile: String,
        email: String,
        password: String,
        avatar: String,
        active: Boolean,
        games: [
          {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Game"
          }
        ]
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    const Player = mongoose.model("player", schema);
    return Player;
  };