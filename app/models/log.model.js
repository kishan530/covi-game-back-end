  module.exports = mongoose => {
    var schema = mongoose.Schema(
      {
        text: String,
        playerLife:String,
        covidLife:String,
        type:String,
        game: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Game"
        },
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    const Log = mongoose.model("Log", schema);
    return Log;
  };