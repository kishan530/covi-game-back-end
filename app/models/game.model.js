  module.exports = mongoose => {
    var schema = mongoose.Schema(
      {
        winner: String,
        date: String,
        duration: String,
        winDuration: String,
        playerLife:String,
        covidLife:String,
        player: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Player"
        },
        logs: [
          {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Log"
          }
        ]
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    const Game = mongoose.model("Game", schema);
    return Game;
  };